import numpy as np
import scipy.linalg as scp
import scipy.sparse as sparse
import scipy.sparse.linalg as slinalg
from dolfin import *
from mshr import *
import progressbar
from ufl import nabla_div
from ufl import nabla_grad

def clamped_boundary(x):
    return x[0] < DOLFIN_EPS

def epsilon(u):
    return 0.5*(nabla_grad(u) + nabla_grad(u).T)

def sigma(u,lambda_,mu,d):
    return lambda_*nabla_div(u)*Identity(d) + 2*mu*epsilon(u)

class high_fidelity():
    def __init__(self):
        # physical parameters
        self.L = 5
        self.W = 1
        self.mu = 1
        self.rho = 1
        self.delta = self.W/self.L
        self.gamma = 0.4*self.delta**2
        self.beta = 1.25
        self.lambda_ = self.beta
        self.g = self.gamma

		# numerical parameters
        self.MAX_ITER = 300000
        self.dt = 0.0001

        #self.construct_mesh()
        #self.save_mesh()

    def initiate_FEM_orig(self):
        self.load_mesh()
        #self.construct_box_mesh()
        #define function space
        self.V = VectorFunctionSpace(self.mesh, 'P', 1)

        #define dirichlet boundary
        self.bc = DirichletBC(self.V, Constant((0, 0)), clamped_boundary)

        #define right hand side function
        self.f = Constant((0, -self.rho*self.g))
        self.T = Constant((0, 0))

        #define functions
        q = TrialFunction(self.V)
        #		p = TrialFunction(self.V)
        self.d = q.geometric_dimension()
        v = TestFunction(self.V)

        # construct FEM matrices
        aq = inner(q,v)*dx
        kq = -inner(sigma(q,self.lambda_,self.mu,self.d),epsilon(v))*dx
        Lq = inner(self.f,v)*dx

        Aq = assemble(aq)
        #self.bc.apply(Aq)
        #self.M = np.matrix( Aq.array() )
        ##self.M_inv = np.linalg.inv(M)
        Kq = assemble(kq)
        #self.bc.apply(Kq)
        #self.K = np.matrix( Kq.array() )

        Aq, bq = assemble_system(aq,Lq,self.bc)
        M = np.matrix( Aq.array() )
        self.M = M
        M = sparse.csc_matrix(M)
        self.sM_inv = slinalg.splu(M)
        Kq, dummy = assemble_system(kq,Lq,self.bc)
        self.K = np.matrix( Kq.array() )

        #self.cp = np.matmul( self.M , np.transpose( np.matrix(bq.get_local()) ) )
        #self.cp = scp.solve(self.M,np.transpose( np.matrix(bq.get_local()) ))
        #self.cp = np.transpose( np.matrix(bq.get_local()) )

        self.cp = np.transpose( np.matrix(bq.get_local()) )

    def construct_mesh(self):
        r1 = Rectangle(Point(0,0),Point(5,1))
        r2 = Rectangle(Point(2.25,0.5),Point(2.75,1))
        geom = r1 - r2
        mesh = generate_mesh(geom,5)

        mf = MeshFunction('bool',mesh, 0)
        mf.set_all(False)
        box = AutoSubDomain(lambda x: x[0] > 0.75 and x[0] < 4.25 )
        box.mark(mf, True)
        mesh = refine(mesh,mf)

        mf = MeshFunction('bool',mesh, 0)
        mf.set_all(False)
        box = AutoSubDomain(lambda x: x[0] > 1 and x[0] < 4 )
        box.mark(mf, True)
        mesh = refine(mesh,mf)

        mf = MeshFunction('bool',mesh, 0)
        mf.set_all(False)
        box = AutoSubDomain(lambda x: x[0] > 1.5 and x[0] < 3.5 )
        box.mark(mf, True)
        mesh = refine(mesh,mf)

        mf = MeshFunction('bool',mesh, 0)
        mf.set_all(False)
        box = AutoSubDomain(lambda x: x[0] > 2 and x[0] < 3 )
        box.mark(mf, True)
        mesh = refine(mesh,mf)

        mf = MeshFunction('bool',mesh, 0)
        mf.set_all(False)
        box = AutoSubDomain(lambda x: (x[0]-2.25)**2 + (x[1]-0.5)**2 < 0.05 )
        box.mark(mf, True)
        box = AutoSubDomain(lambda x: (x[0]-2.75)**2 + (x[1]-0.5)**2 < 0.05 )
        box.mark(mf, True)
        box = AutoSubDomain(lambda x: (x[0]-2.25)**2 + (x[1]-1)**2 < 0.05 )
        box.mark(mf, True)
        box = AutoSubDomain(lambda x: (x[0]-2.75)**2 + (x[1]-1)**2 < 0.05 )
        box.mark(mf, True)
        mesh = refine(mesh,mf)

        mf = MeshFunction('bool',mesh, 0)
        mf.set_all(False)
        box = AutoSubDomain(lambda x: (x[0]-2.25)**2 + (x[1]-0.5)**2 < 0.01 )
        box.mark(mf, True)
        box = AutoSubDomain(lambda x: (x[0]-2.75)**2 + (x[1]-0.5)**2 < 0.01 )
        box.mark(mf, True)
        box = AutoSubDomain(lambda x: (x[0]-2.25)**2 + (x[1]-1)**2 < 0.01 )
        box.mark(mf, True)
        box = AutoSubDomain(lambda x: (x[0]-2.75)**2 + (x[1]-1)**2 < 0.01 )
        box.mark(mf, True)
        mesh = refine(mesh,mf)

        mf = MeshFunction('bool',mesh, 0)
        mf.set_all(False)
        box = AutoSubDomain(lambda x: (x[0]-2.25)**2 + (x[1]-0.5)**2 < 0.002 )
        box.mark(mf, True)
        box = AutoSubDomain(lambda x: (x[0]-2.75)**2 + (x[1]-0.5)**2 < 0.002 )
        box.mark(mf, True)
        box = AutoSubDomain(lambda x: (x[0]-2.25)**2 + (x[1]-1)**2 < 0.002 )
        box.mark(mf, True)
        box = AutoSubDomain(lambda x: (x[0]-2.75)**2 + (x[1]-1)**2 < 0.002 )
        box.mark(mf, True)
        self.mesh = refine(mesh,mf)

    def construct_box_mesh(self):
        r1 = Rectangle(Point(0,0),Point(5,1))
        geom = r1
        mesh = generate_mesh(geom,5)

        mf = MeshFunction('bool',mesh, 0)
        mf.set_all(False)
        box = AutoSubDomain(lambda x: x[0] > 0.75 and x[0] < 4.25 )
        box.mark(mf, True)
        mesh = refine(mesh,mf)

        mf = MeshFunction('bool',mesh, 0)
        mf.set_all(False)
        box = AutoSubDomain(lambda x: x[0] > 1 and x[0] < 4 )
        box.mark(mf, True)
        self.mesh = refine(mesh,mf)
        #r = Rectangle(Point(0,0),Point(5,1))
        #self.mesh = generate_mesh(r,10)


    def stormer_verlet( self ):
        vtkfile = File('results/solution.pvd')
        f = Function(self.V)

        N = self.cp.shape[0]
        q0 = np.zeros([N,1])
        p0 = np.zeros([N,1])
        #q0, p0 = self.load_initial_condition(q0,p0)

        self.snap_Q = np.zeros( self.cp.shape )
        self.snap_P = np.zeros( self.cp.shape )

        #print('I am here')
        #input()
        #P, L, U = scp.lu(self.M)
        #P = np.transpose(P)

        for i in progressbar.progressbar(range(0,self.MAX_ITER)):
        #for i in range(0,self.MAX_ITER):
            #temp = scp.solve_triangular(L,np.matmul(P,p0),lower=True)
            #temp = scp.solve_triangular(U,temp)
            #q0 = q0 + self.dt/2*temp
            #p0 = p0 + self.dt*np.matmul(self.K,q0) + self.dt*self.cp
            #temp = scp.solve_triangular(L,np.matmul(P,p0),lower=True)
            #temp = scp.solve_triangular(U,temp)
            #q0 = q0 + self.dt/2*temp
            #q0 = q0 + self.dt/2*np.matmul(self.M,p0)
            #p0 = p0 + self.dt*np.matmul(self.K,q0) + self.dt*self.cp
            #q0 = q0 + self.dt/2*np.matmul(self.M,p0)
            p0 = p0 + self.dt/2*np.matmul(self.K,q0) + self.dt/2*self.cp
            #temp = scp.solve_triangular(L,np.matmul(P,p0),lower=True)
            #temp = scp.solve_triangular(U,temp)
            temp = self.sM_inv.solve( p0 )
            q0 = q0 + self.dt*temp
            p0 = p0 + self.dt/2*np.matmul(self.K,q0) + self.dt/2*self.cp

            #self.compute_energy(q0,p0)

            #f.vector().set_local( q0 )
            if np.mod(i,1000) == 0:
                self.snap_Q = np.concatenate((self.snap_Q,q0),1)
                self.snap_P = np.concatenate((self.snap_P,p0),1)
                #vtkfile << (f,i*self.dt)

    def euler( self ):
        vtkfile = File('results/solution.pvd')
        f = Function(self.V)

        N = self.cp.shape[0]
        q0 = np.zeros([N,1])
        p0 = np.zeros([N,1])

        self.snap_Q = np.zeros( self.cp.shape )
        self.snap_P = np.zeros( self.cp.shape )

        #P, L, U = scp.lu(self.M)
        #P = np.transpose(P)

        for i in progressbar.progressbar(range(0,self.MAX_ITER)):
        #for i in range(0,self.MAX_ITER):
            #temp = scp.solve_triangular(L,np.matmul(P,p0),lower=True)
            #temp = scp.solve_triangular(U,temp)
            temp = self.sM_inv.solve( p0 )
            q0 = q0 + self.dt*temp
            #p0 = p0 + self.dt*np.matmul(self.K,q0) + self.dt*self.cp
            #q0 = q0 + self.dt*np.matmul(self.M,p0)
            p0 = p0 + self.dt*np.matmul(self.K,q0) + self.dt*self.cp
            #p0 = np.matmul(self.M,p0)

            #self.compute_energy(q0,p0)

            f.vector().set_local( q0 )
            if np.mod(i,1000) == 0:
                self.snap_Q = np.concatenate((self.snap_Q,q0),1)
                self.snap_P = np.concatenate((self.snap_P,p0),1)
                #vtkfile << (f,i*self.dt)

    def save_mesh(self):
        file_name = 'mesh.xml'
        File(file_name) << self.mesh

    def load_mesh(self):
        file_name = 'mesh.xml'
        self.mesh = Mesh(file_name)

    def load_initial_condition(self,q0,p0):
        file_name = 'init_cond.npz'
        file = np.load(file_name)
        q = file['q0']
        p = file['p0']
        q0 = np.transpose( np.matrix( q ) )
        p0 = np.transpose( np.matrix( p ) )
        return q0,p0


    def save_snapshots(self):
        file='data.npz'
        np.savez(file, snap_Q=self.snap_Q, snap_P=self.snap_P)

    def save_from_data(self):
        data = np.load('temp_data.npz')
        self.snap_Q = data['snap_Q']

        vtkfile = File('results/solution.pvd')
        f = Function(self.V)
        for i in progressbar.progressbar(range(0,self.snap_Q.shape[1])):
        #for i in range(0,self.snap_Q.shape[1]):
            if( i%10 == 0):
                q = self.snap_Q[:,i]
                f.vector().set_local( q )
                vtkfile << (f,i*self.dt)
        print(self.snap_Q.shape)

    def compute_X_mat(self):
        #N = self.K.shape[0]
        #self.X = np.zeros([2*N,2*N])
        #self.X[0:N,0:N] = -self.K/2
        #self.X[N:2*N,N:2*N] = self.M/2

        file_name = 'X_mat.npz'
        np.savez(file_name,K=self.K,c=self.cp,M=self.M)

    def compute_energy(self,q,p):
        #q0 = Function( self.V )
        #q0.vector().set_local( q )
        #p0 = Function( self.V )
        #p0.vector().set_local( p )

        #energy = - inner( self.f,q0 )*dx# 0.5*inner(p0,p0)*dx# + 0.5*inner(sigma(q0,self.lambda_,self.mu,self.d),epsilon(q0))*dx - inner( self.f,q0 )*dx
        #E = assemble(energy)

        #temp = np.matmul(self.M,p)/2
        temp = self.sM_inv.solve(p)/2
        kin = np.matmul(np.transpose(p),temp)
        temp = - np.matmul(self.K,q)
        pot = np.matmul(np.transpose(q),temp )/2
        force = -np.matmul(np.transpose(q),self.cp)
        print(kin + pot + force)

        #N = self.K.shape[0]
        #z = np.zeros([2*N,1])
        #z[0:N] = q
        #z[N:2*N] = p
        #temp = np.matmul(self.X,z)
        #E = np.matmul(np.transpose(z),temp)
        #force = -np.matmul(np.transpose(q),self.cp)
        #print(E+force)

if __name__ == '__main__':
    hf = high_fidelity()
    print('Initiating FEM')
    hf.initiate_FEM_orig()
    #print('Time integration')
    #hf.stormer_verlet()
    #hf.euler()
    #print('Saving data')
    #hf.save_snapshots()
    #print('Saving X matrix')
    #hf.compute_X_mat()

    hf.save_from_data()
